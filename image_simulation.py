import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import time
import numpy as np
import seaborn as sns

sns.set()

img = mpimg.imread("circuit_2.png")

img_1d = img[:,:,0]

def distance(position_1, position_2):
    """Calcul la distance entre deux position"""
    x_1, y_1 = position_1
    x_2, y_2 = position_2
    return np.linalg.norm([x_1 - x_2, y_1 - y_2])
    
class Voiture:
    """Object Voiture"""
    def __init__(self, position, direction, speed):
        self.position = position
        self.direction = direction
        self.speed = speed

    def update_direction(self, points):
        """Algo des chages fictives"""
        # changement de repère
        # points = points - self.position
        # ajout d'un point fictif
        # points = np.append(points, [0, -0.1])
        # points = np.reshape(points, (-1,2))

        avg_x = 0
        avg_y = 0

        for p in points:
            x, y = p
            r = distance(p, [0,0])
            # theta = np.arctan2(y, x)
            avg_x -= x / (r**2)
            avg_y -= y / (r**2)
            
        direction_cf = np.array([avg_x, avg_y])
        direction_cf = direction_cf / np.linalg.norm(direction_cf)

        self.direction = direction_cf

    def next_position(self):
        """Calcul de la prochaine position"""
        return self.position + self.speed * np.array([self.direction[1],
                                                      self.direction[0]])

class Lidar:
    """Objet Lidar"""
    def __init__(self, circuit, radius):
        self.circuit = circuit
        self.radius = radius

    def list_points(self, position):
        """renvoie la liste des points autour d'une certaine position"""
        points = []
        x, y = position
        r = self.radius
        x_max, y_max = 400, 400
        for i in range(max(int(x - r), 0), min(int(x + r), x_max - 1)):
            for j in range(max(int(y - r), 0), min(int(y + r), y_max - 1)):
                if distance([i,j], position) < self.radius:
                    if self.circuit[i, j] == 0:
                        points.append([i, j])

        return np.array(points)

fig, ax = plt.subplots()

plt.imshow(img_1d, cmap=plt.get_cmap('gray'))

# défintion du lidar
lidar = Lidar(img_1d, 100)

# défintion de la voiture
pos_ini = np.array([60, 64])
dir_ini = np.array([1.0, 0.0])
speed = 5.0
voiture = Voiture(pos_ini, dir_ini, speed)


def plot_point(point, c="blue"):
    plt.scatter(point[0], point[1], c=c)


def plot_direction(a, b):
    """plot arrow from a with direciton b"""
    plt.quiver(a[0], a[1], b[0], b[1])


L_p = []
L_d = []
k_max = 10

for k in range(k_max):
    print(k)
    #print(voiture.position, voiture.direction)
    donnees_lidar = lidar.list_points(voiture.position)
    voiture.update_direction(donnees_lidar - voiture.position)
    voiture.position = voiture.next_position()
    L_p.append(voiture.position)
    L_d.append(voiture.direction)
    #plot_point(voiture.position)
    #plot_direction(voiture.position, voiture.next_position())

L_p = np.array(L_p)
L_d = np.array(L_d)

plt.quiver(L_p[:,0], L_p[:,1], L_d[:,0], L_d[:,1], units="xy")
plt.scatter(L_p[:,0], L_p[:,1], c=list(range(k_max)))
plt.scatter(donnees_lidar[:,0], donnees_lidar[:,1], c="red")

print(donnees_lidar)

ax.set_aspect("equal")
plt.show()




"""
k_max = 10

for k in range(k_max):
    print("itération : ", k)
    print("position : ", voiture.position)
    print("direction : ",voiture.direction)
    donnees_lidar = lidar.list_points(voiture.position)
    # print(donnees_lidar)
    voiture.position = voiture.next_position()
    voiture.update_direction(donnees_lidar) 

    list_pos_voiture.append(voiture.position)

list_pos_voiture = np.array(list_pos_voiture)
plt.scatter(list_pos_voiture[:,0], list_pos_voiture[:,1], c=list(range(k_max)))
#plt.scatter(donnees_lidar[:,0], donnees_lidar[:,1], color="red")
plt.show()
"""
"""
for position in [[60,64], [165,41], [115, 355], [344, 193]]:
    plt.figure()
    plt.imshow(img_1d, cmap=plt.get_cmap('gray'))
    donnees_lidar = lidar.list_points(position)
    plt.scatter(position[0], position[1])
    n_pos = position + voiture.speed * voiture.direction
    plt.scatter(n_pos[0], n_pos[1],
                color="black")
    plt.scatter(donnees_lidar[:,0], donnees_lidar[:,1], color="red")
    plt.show(block=False)
plt.show()
"""

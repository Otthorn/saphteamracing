#!/bin/env python3
# author : Solal Nathan

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import seaborn as sns
sns.set()

def import_image(path):
    """Imports the data from an image and converts it to the right shape."""
    src = mpimg.imread(path)
    img = src[:,:,0]
    data = np.where(img == 0)
    data = np.reshape(data, (2, -1))
    return data.T


def rotation_matrix(theta):
    """Renvoie la matrice de rotation 2D associé à un angle donné."""
    R = np.array([[np.cos(theta), -np.sin(theta)],
                  [np.sin(theta), np.cos(theta)]])
    return R


def rotate_ciruit(data, R):
    """Effectue une rotation d'un ensemble par la matrice de rotation R.
    data est un ensemble de points de la forme :
        data = [[x_0, y_0],
                [x_1, y_1],
                   ...
                [x_n, y_n]]
    """
    data_r = np.dot(R, data.T)
    data_r = np.reshape(data_r, (2, -1))
    return data_r.T


def translate_circuit(data, point):
    """Effectue la translation d'un ensemble de points"""
    return data - point


def distance(point_1, point_2):
    """Calcul de la distance entre deux points"""
    x_1, y_1 = point_1
    x_2, y_2 = point_2
    return np.linalg.norm([x_1 - x_2, y_1 - y_2])

def algo_cf(ensemble_points):
    avg_x = 0
    avg_y = 0

    for p in ensemble_points:
        x, y = p
        r = distance(p, [0, 0])
        if r < 100:
            theta = np.arctan2(y, x)
            puissance = heuristic_puissance(theta)
            avg_x -= x / r**puissance
            avg_y -= y / r**puissance
        
    dcf = np.array([avg_x, avg_y])
    return dcf / np.linalg.norm(dcf)

def heuristic_puissance(theta):
    return 0.5*np.cos(2*theta) + 1.5


def change_ref(position, direction, circuit):
    """fonction temporaire"""
    circuit = translate_circuit(circuit, position)

    phi = np.pi/2 - np.arctan2(direction[1], direction[0])

    R = rotation_matrix(phi)
    circuit = rotate_ciruit(circuit, R)

    return circuit

def plot_circuit(circuit):
    """fonction temporaire; à suprimmer"""

    #plt.figure()
    plt.scatter(circuit[:,0], circuit[:,1])
    plt.scatter(0, 0, marker="x", color="red")
    plt.quiver(0, 0, 0, 1, units="xy")
    plt.show(block=False)
    plt.pause(0.05)
    plt.clf()



circuit = import_image("circuit_2.png")

# donnes de la voiture
position = np.array([45, 200])
direction = np.array([0, 1])
speed = 5.0

points = []

for k in range(1):
    print(k)
    circuit_ref_voiture = change_ref(position, direction, circuit) 
    direction_new = algo_cf(circuit)
    position = position + speed*direction_new
    # plot_circuit(circuit_ref_voiture)


#plot_circuit([45, 214], [0, 1], circuit)
#plot_circuit([45, 171], [0, 1], circuit)
#plot_circuit([75, 84], [-1, 1], circuit)
#plt.show()

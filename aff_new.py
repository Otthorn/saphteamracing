import numpy as np
import matplotlib.pyplot as plt

fig = plt.figure()
ax = fig.gca(projection='polar')
ax.grid(True)
ax.set_rmax(15)

tableau = np.loadtxt('teraterm3.log', delimiter=',')
angle = tableau[:,0]
distance = tableau[:,1]

angle_r = np.pi/180*angle

ax.scatter(angle_r, distance)
plt.show()

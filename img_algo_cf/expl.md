## r2

 * 1,2,3 : exemples sur des points aléatoire
 * 5 : exemple sur un chemin parfaitement droit (ça va droit)
 * 4 : exemple avec un peu de bruit (ça va plus droit)
 * 6 : exemple proche d'un bord (trop d'importance aux points sur le côté)
 * champ_cf : champ de la direction à prendre selon l'algo calculer en tout
   points (discretiser)
 * on calcul aussi le champs pour $r^1$ et $r^3$.. ça change pas grand chose

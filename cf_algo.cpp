#include <stdio.h>      /* printf */
#include <math.h>       /* pow */

float distance(float x_1, float x_2, float y_1, float y_2)
{
    float norm2;
    norm2 = pow(pow(x_1 - x_2, 2) + pow(y_1 - y_2, 2), 0.5);
    return norm2;
}

void update_direction(int* list_lidar, float* vecteur)
{
    float direction[2];

    direction[0] = 0;
    direction[1] = 1;

    float avg_x, avg_y, sum_inv_dist;
    list_lidar[180] = 50; // [mm], point fictif qui pousse la voiture
    int i;
    avg_x = 0;
    avg_y = 0;

    for (i=0; i<360; i++)
    {
        int theta;
        float r, x, y;

        theta = i;
        r = list_lidar[theta];

        x = r*cos(theta);
        y = r*sin(theta);

        sum_inv_dist += 1/pow(r, 2);
        avg_x -= x/sum_inv_dist;
        avg_y -= y/sum_inv_dist;
    }
    
    avg_x /= sum_inv_dist;
    avg_y /= sum_inv_dist;   
    direction[0] = avg_x;
    direction[1] = avg_y;

    for(i=0; i<2; i++)
        vecteur[i] = direction[i];
}

float angle_servo(float *direction)
{
    float angle, pwm;
    float x, y;
    
    x = direction[0];
    y = direction[1];
 
    angle = atan(x/y);
    pwm = 14.662756 * angle - 1453.08;

    return pwm;
}

int main()
{ 
    float dir[2];
    float pwm;
    int list_lidar[360];
    int i;
    // remplissage fictif du tableau
    for (i=0; i<360; i++)
        list_lidar[i] = 100;
   
    update_direction(list_lidar, dir);
    pwm = angle_servo(dir);

    printf("direction = %f, %f", dir[0], dir[1]);
    printf("\n");
    printf("pwm = %f", pwm);
    printf("\n");

}


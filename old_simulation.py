# -*- coding: utf-8 -*-
"""
Projet Saph-team Racing

Simulation de l'algorithme d'évitement d'obstacles
"""

from math import *
import matplotlib.pyplot as plt
import numpy as np
import time

def distance(point1, point2) :
    """
    Renvoie la distance en norme 2 entre les deux points
    """
    x1, y1 = point1
    x2, y2 = point2
    return sqrt((x1-x2)**2+(y1-y2)**2)
    
class Circuit() :
    """
    Objet modélisant un circuit de course composé de deux rectangles 
    cocentriques de tailles différentes
    """
    
    def __init__(self, x_paroi_int, y_paroi_int, x_paroi_ext, y_paroi_ext) :
        """
        Un ciruit possède 5 attributs :
            - x_paroi_int : demi-largeur du rectangle intérieur selon x, float
            - y_paroi_int
            - x_paroi_ext
            - y_paroi_ext
            - coordonnées du point de départ choisi arbitrairement à égale 
            distance des deux rectangles
        """
        self.x_paroi_int = x_paroi_int
        self.y_paroi_int = y_paroi_int
        self.x_paroi_ext = x_paroi_ext
        self.y_paroi_ext = y_paroi_ext
        y_depart = 0 
        x_depart = (x_paroi_int + x_paroi_ext)/2
        self.pos_depart = np.array([x_depart, y_depart])
        
    def __str__(self) :
        return "Circuit entre les abscisses {} et {} et les ordonnées {} et {}"\
    .format(self.x_paroi_int, self.x_paroi_ext, self.y_paroi_int,
    self.y_paroi_ext)
    
    def __repr__(self) :
        return "Circuit : {}".format(self)
    
    def is_in_circuit(self, point) :
        """
        point : liste de deux coordonnées, renvoie True si le point est dans le
        circuit.
        """
        x, y = point
        x_min, x_max = self.x_paroi_int, self.x_paroi_ext
        y_min, y_max = self.y_paroi_int, self.y_paroi_ext
        if abs(x) > x_max or abs(y) > y_max :
            return False
        if abs(x) < x_min and abs(y) < y_min :
            return False
        return True
    
    

class Lidar() :
    """
    Objet modélisant un lidar qui communique avec une voiture et qui repère
    des points sur un circuit.
    """
    
    def __init__(self, circuit, voiture) :
        """
        L'objet lidar possède 4 attributs :
            - taux_raffraichissement : int, nombre d'itérations avant de 
            rafraîchir la liste des points perçus
            - distance_perception : distance à laquelle le lidar perçoit les
            points du circuit
            - resolution angulaire
            - circuit
            - voiture
        """
        self.circuit = circuit
        self.voiture = voiture
        self.refresh_time = 1
        self.d_perception = 10
        self.resolution_deg = 4
        
    def __str__(self) :
        return "Lidar de la voiture {}".format(self.voiture)
    
    def __repr__(self) :
        return "Lidar : {}".format(self)
    
    def capter_points(self) :
        circuit = self.circuit
        voiture = self.voiture
        pos = voiture.pos
        x_v, y_v = pos
        
        #Création du cercle des points visibles les plus éloignés
        points_percus = []
        r = self.d_perception
        alpha = 0 #Angle repérant le point considéré par rapport à la verticale
        while alpha < 2*pi :
            x = x_v + r * sin(alpha)
            y = y_v + r * cos(alpha)
            point = [x,y]
            
            #Pour chaque point, vérifier s'il est hors circuit et le remplacer
            #par le point du mur correspondant le cas échéant.
            if not circuit.is_in_circuit(point) :
                distance_point = 0
                point_in_circuit = True
                while point_in_circuit and distance_point < r :
                    distance_point += r / 100
                    point = [x_v + distance_point * sin(alpha), y_v + \
                             distance_point * cos(alpha)]
                    point_in_circuit = circuit.is_in_circuit(point)
                points_percus.append(point)
            
            #Incrémentation
            alpha += self.resolution_deg * pi / 180
            
        return points_percus
                
                
        

class Voiture() : 
    """
    Objet modélisant la voiture, communique avec le lidar et évolue sur un 
    circuit.
    """
    
    def __init__(self, circuit) :
        """
        L'objet voiture possède 4 attributs :
            - Un vecteur de direction, liste de deux éléments, dans le 
            référentiel de la voiture
            - Un circuit sur laquelle elle évolue 
            - Un lidar avec lequel elle communique
            - Des coordonnées sur le circuit (list de deux floats)
            - size : largeur et longueur de la voiture
        """
        self.direction = [0,1]
        self.circuit = circuit
        #self.lidar = lidar
        self.pos = circuit.pos_depart #À terme circuit.depart
        self.size = [5,10]
        self.direction = np.array([0.,5.])
        self.moyennex = 0
        self.moyenney = 0
    
    def __str__(self) :
        return "Coordonnées : {} sur le circuit {} \nVitesse : {} m/s\nAngle des roues : {} deg"\
               .format(self.pos, self.circuit)

    def __repr__(self) :
        return "Voiture : {}".format(self)
    
    def maj_trajectoire(self, liste_points_percus) :
        """
        liste_points_percus : liste de coordonnées des points perçus par le
        lidar dans le référentiel global.
        Renvoie une vitesse et une orientation mises à jour à l'aide d'un 
        l'algorithme de calcul de trajectoire.
        """
        liste_points_percus += [voiture.pos-voiture.direction*10]
        liste_points_percus = np.array(liste_points_percus)
        liste_points_percus -= voiture.pos
        moyenne_x = 0
        moyenne_y = 0
        somme_distances_inv = 0
        for point in liste_points_percus :
            x, y = point
            distance_point = distance([0,0], point) ** 2
            somme_distances_inv += 1/distance_point
            moyenne_x -= x / distance_point
            moyenne_y -= y / distance_point
        moyenne_x /= somme_distances_inv
        moyenne_y /= somme_distances_inv
        self.direction = np.array([moyenne_x,moyenne_y])/10
        self.moyennex = moyenne_x
        self.moyenney = moyenne_y
        
    def maj_pos(self):
        """
        Met à jour la position de la voiture à partir de sa direction.
        """
        direction = self.direction
        norme = sqrt(direction[0]**2 + direction[1]**2)
        self.pos += self.direction/norme*2
        
        

#Test
#voiture = Voiture("c")
#liste_points_percus = [[1,-5],[1,-2.5],[1,0],[1,2.5],[1,5],[0,5],[-1,5],[-2.5,5],[100,100]]
#liste_points_percus = [[-5,-5],[-5,-2.5],[-5,0],[-50,2.5],[-50,5],[5,-5],[5,-2.5],[5,0],[5,2.5],[5,5],[100,100]]
#for t in range(10) :    
#    plt.figure("{}".format(t))
#    liste_points_percus.pop()
#    for point in liste_points_percus :
#        plt.plot(point[0],point[1],"or")
#    plt.plot(0,0,"o", label = "voiture".format(t))
#    plt.plot(voiture.direction[0], voiture.direction[1], "yD", label = "direction {}".format(t))
#    plt.plot(voiture.moyennex, voiture.moyenney, "o",label = "moyenne {}".format(t))
#    plt.legend()
#    plt.show()
#    
#    voiture.pos += voiture.direction
#    voiture.maj_trajectoire(liste_points_percus)
    

x_min, x_max, y_min, y_max = 10, 20, 10, 20
circuit = Circuit(x_min,y_min,x_max, y_max)
voiture = Voiture(circuit)
lidar = Lidar(circuit, voiture)
periode_maj = lidar.refresh_time

plt.plot(voiture.pos[0], voiture.pos[1],'yo')
plt.plot([-x_min,x_min,x_min,-x_min,-x_min],[-y_min,-y_min,y_min,y_min,-y_min],'r')
plt.plot([-x_max,x_max,x_max,-x_max,-x_max],[-y_max,-y_max,y_max,y_max,-y_max],'r')
plt.axis('equal')
plt.grid()

for t in range(1,10) :
    if t % periode_maj == 0 :
        points_percus = lidar.capter_points()
        voiture.maj_trajectoire(points_percus)
        direction = voiture.direction
        #plt.quiver(*voiture.pos, direction[0], direction[1], angles="xy", scale_units='xy', scale=1)
        plt.quiver(*voiture.pos, direction[0], direction[1], scale = 1)
        
    voiture.maj_pos()
    x_v, y_v = voiture.pos
    plt.plot(x_v, y_v, 'D', label = "{}".format(t))
    #ax = plt.axes()
    #ax.arrow(voiture.pos[0], voiture.pos[1], *direction, head_width = 0.01)

X = [point[0] for point in points_percus]
Y = [point[1] for point in points_percus]
plt.plot(X,Y,'bo')

plt.legend()
plt.show()


















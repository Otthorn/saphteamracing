#!/bin/env python3
import matplotlib.pyplot as plt
import numpy as np

def distance(position_1, position_2):
    """Calcul la distance entre deux position"""
    x_1, y_1 = position_1
    x_2, y_2 = position_2
    return np.linalg.norm([x_1 - x_2, y_1 - y_2])

def algo_cf(ensemble_points):

    # changement de référentel
    #ensemble_points = ensemble_points -  position
    # ajout d'un point fictif

    avg_x = 0
    avg_y = 0

    for p in ensemble_points:
        x, y = p
        r = distance(p, [0,0])
        #theta = np.arctan2(y, x)
        avg_x -= x / (r**2)
        avg_y -= y / (r**2)

    direction_cf = np.array([avg_x, avg_y])
    direction_cf = direction_cf / np.linalg.norm(direction_cf)
    return direction_cf

fig, ax = plt.subplots()

points_1 = np.array([[49, 0.21], [83, 20], [-12, -19]])
plt.scatter(points_1[:,0], points_1[:,1], c="blue")

X = np.linspace(-100, 100, 20)
Y = np.linspace(-100, 100, 20)

print("Progression :")

for x in X:
    print(f"{x/1.7:.2f} %")
    for y in Y:
        origin = np.array([x, y])
        direction = algo_cf(points_1 - origin)
        #d.append(direction)  
        plt.quiver(x, y, direction[0], direction[1], units="xy")


#origin = np.array([42, 3])
#direction =  algo_cf(points_1 - origin)
#plt.quiver(origin[0], origin[1], direction[0], direction[1], units="xy")
#plt.scatter(origin[0], origin[1], c="orange")

plt.grid()
plt.xlim(-100, 100)
plt.ylim(-100, 100)
ax.set_aspect('equal')
plt.show()
# Plot dans le changement de repère
"""
fig, ax = plt.subplots()
plt.xlim(-100, 100)
plt.ylim(-100, 100)

origin = np.array([42, 3])

points = points_1 - origin

direction = algo_cf([0, 0], points)
plt.quiver(0, 0, direction[0], direction[1])
plt.scatter(points[:,0], points[:,1], color="blue")
plt.scatter(0, 0, color="orange")



plt.grid()

ax.set_aspect('equal')
plt.show()
"""

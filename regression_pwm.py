import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
sns.set(color_codes=True)
from scipy import stats

data = np.array([[1370, 0],
                [1421, -4],
                [1472, -10],
                [1523, -15],
                [1574, -21],
                [1625, -24],
                [1319, 5],
                [1268, 11],
                [1217, 16],
                [1166, 20],
                [1115, 24]])



sns.regplot(data[:,1], data[:,0], x_estimator=np.mean)
plt.xlabel(r"$\theta$ (°)")
plt.ylabel("PWM")

y = data[:,0]
x = data[:,1]
slope, intercept, r_value, p_value, std_err = stats.linregress(x, y)
#plt.scatter(x, y, label="donnes experimentales")
#plt.plot(x, intercept + slope*x, 'r', label="fit")
plt.show()


print(slope, intercept, r_value, p_value, std_err)

print(f"pwm = {slope} * angle + {intercept}")
print("angle en degree")

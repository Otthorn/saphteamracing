import tkinter as tk

from simulation import Voiture

Car = Voiture("test")

class Application(tk.Frame):
    def __init__(self, master=None):
        super().__init__(master)
        self.master = master
        self.pack()

        master.geometry("500x300")
        self.create_canvas()
        self.draw_circuit(20, 20, 100, 100)
        self.draw_car(Car.pos, Car.size)
    
    def create_canvas(self):
        self.master.canvas = tk.Canvas(self, width=520, height=320)
        self.master.canvas.pack()

    def draw_circuit(self, in_x, in_y, out_x, out_y):
        
        H = 500
        L = 300

        in_x_1 = H/2 - in_x
        in_y_1 = L/2 - in_y
        in_x_2 = H/2 + in_x
        in_y_2 = L/2 + in_y

        out_x_1 = H/2 - out_x
        out_y_1 = L/2 - out_y
        out_x_2 = H/2 + out_x
        out_y_2 = L/2 + out_y

        outter_border = self.master.canvas.create_rectangle(out_x_1, out_y_1, out_x_2, out_y_2, fill="grey")
        inner_border = self.master.canvas.create_rectangle(in_x_1, in_y_1, in_x_2, in_y_2, fill="white")

    def draw_car(self, pos, size):
        
        H = 500
        L = 300
       
        pos_x = pos[0]
        pos_y = pos[1]

        size_x = size[0]
        size_y = size[1]

        x_1 = H/2 + pos_x - size_x/2
        y_1 = L/2 - size_y/2

        x_2 = H/2 + pos_x + size_x/2
        y_2 = L/2 + size_y/2
 
        self.master.canvas.create_rectangle(x_1, y_1, x_2, y_2, fill="red")

    def move_car(direction):
        if direction == "up":
            Car.pos[0] += 0.1 
            
    def mainloop(self):
        print("mainloop")
        super().mainloop()
        self.draw_car(Car.pos, Car.size)

root = tk.Tk()


app = Application(master=root)

root.bind('<z>', Application.move_car("up"))
root.bind('<q>', Application.move_car("left"))
root.bind('<s>', Application.move_car("down"))
root.bind('<d>', Application.move_car("right"))

app.mainloop()

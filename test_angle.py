import numpy as np

def cart2pol(x, y):
    r = (x**2 + y**2)**(1/2)
    theta = np.arctan2(y, x)
    return r, theta

def pol2cart(r, theta):
    x = r*np.cos(theta)
    y = r*np.sin(theta)
    return x, y

def unit_vector(vector):
    """ Returns the unit vector of the vector."""
    return vector / np.linalg.norm(vector)

def angle_between(v1, v2):
    """ Returns the angle in radians between vectors 'v1' and 'v2'"""
    v1_u = unit_vector(v1)
    v2_u = unit_vector(v2)
    return np.arccos(np.clip(np.dot(v1_u, v2_u), -1.0, 1.0))

v1 = [1, 0]
v2 = [1, 1]
v3 = [-1, 1]

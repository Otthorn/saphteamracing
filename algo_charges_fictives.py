#!/bin/env python3
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import numpy as np
import seaborn as sns

sns.set()

def distance(position_1, position_2):
    """Calcul la distance entre deux position"""
    x_1, y_1 = position_1
    x_2, y_2 = position_2
    return np.linalg.norm([x_1 - x_2, y_1 - y_2])

def rotation_matrix(phi):
    """Renvoie la matrice de rotation"""
    R = np.array([[np.cos(phi), -np.sin(phi)],
                 [np.sin(phi), np.cos(phi)]])
    return R


def algo_cf(ensemble_points, direction):
    
    old_direction = np.copy(direction)
    #phi = angle_between(old_direction, np.array([0,1]))
    #phi = - phi
    #print(f"phi = {180/np.pi*phi}")

    #R = rotation_matrix(phi)
    #R = R.T
    
    avg_x = 0
    avg_y = 0
    
    for p in ensemble_points:
        x, y = p
        r = distance(p, [0,0])
        if r > 100: # on ajoute le lidar
            pass
        else:
            #x_p, y_p = np.dot(R, [x, y]) # rotation
            x_p, y_p = x, y
            theta = np.arctan2(y_p, x_p)
            avg_x -= x_p / (r**(0.5*np.cos(2*theta) + 1.5))
            avg_y -= y_p / (r**(0.5*np.cos(2*theta) + 1.5))

    direction_cf = np.array([avg_x, avg_y])
    #direction_cf = np.dot(R.T, direction_cf) # rotation inverse
    direction_cf = direction_cf / np.linalg.norm(direction_cf)
    
    # heuristique d'inertie
    alpha = 0.2
    direction_cf = alpha*direction_cf + (1-alpha)*old_direction

    return direction_cf

fig, ax = plt.subplots()


def unit_vector(vector):
    """ Returns the unit vector of the vector."""
    return vector / np.linalg.norm(vector)

def angle_between(v1, v2):
    """ Returns the angle in radians between vectors 'v1' and 'v2'"""
    v1_u = unit_vector(v1)
    v2_u = unit_vector(v2)
    return np.arccos(np.clip(np.dot(v1_u, v2_u), -1.0, 1.0))

img_src = mpimg.imread("circuit_2.png")
img = img_src[:, :, 0]
img = np.where(img == 0)
A = np.reshape(img, (2, -1))
img = A.T

#plt.scatter(img[:,0], img[:,1])

position = np.array([45, 200])
direction = np.array([0, 1])
speed = 5.0

k_max = 5
for k in range(k_max):
    print(f"{k/k_max*100:.2f} %")
    #print(position)
    position = position + speed * direction

    phi = angle_between(direction, [0,1])
    R = rotation_matrix(phi)

    # changement de référentiel dans le reférientiel de la voiture
    img_v = img - position
    # rotation
    img_r = np.dot(R, img_v.T)
    img_r = np.reshape(img_r, (2, -1))
    img_r = img_r.T

    #direction = algo_cf(img - position, direction)
    direction = algo_cf(img_r, [0, 0])
    # rotation dans l'autre sens
    direction

    plt.scatter(0, 0, c="red")
    plt.scatter(img_r[:,0], img_r[:,1])
    plt.quiver(0, 0, direction[0], direction[1], units="xy")

    plt.grid()
    ax.set_aspect('equal')
    plt.show(block=False)
    plt.pause(0.01)

plt.show()

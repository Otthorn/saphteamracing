import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
sns.set()

def h1(theta):
    return 0.5*np.cos(2*theta) + 1.5

theta = np.linspace(0, 2*np.pi, 100)
h1 = h1(theta)

fig = plt.figure()
ax = fig.gca(projection='polar')

t20 = np.linspace(0, 2*np.pi, 20)

ax.plot(theta, h1)
ax.plot(theta, 100*[1], "r--")
ax.plot(theta, 100*[2], "r--")
ax.set_theta_zero_location("N")
ax.set_rmax(2.5)
ax.set_rmin(0)
#plt.xlabel(r"$\theta$")
#plt.ylabel("$h$")
plt.show()
